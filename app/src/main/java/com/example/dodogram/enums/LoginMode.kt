package com.example.dodogram.enums

enum class LoginMode{
    GOOGLE,
    FACEBOOK,
    EMAIL,
    PHONE,
    NONE
}