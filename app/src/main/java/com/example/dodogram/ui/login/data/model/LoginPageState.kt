package com.example.dodogram.ui.login.data.model

data class LoginPageState(
    val isLoginPageVisible:Boolean = true,
    val isRegisterPageVisible:Boolean = false
)
